$(document).ready(function() {

	var timer = 0;
	var MAX_CALLS 	= 20;
	var AJAX_INTERVAL = 3000;
	var TIMER_VALUE = MAX_CALLS*AJAX_INTERVAL/1000;
	var stop_ajax = false;

	function get_other_screenshots(number) {

		$.ajax({
			type: 		"POST",
			url: 		'ajax/get-more-screenshots/',
			data: 		{number: number},
			timeout: 	10000
		})
		.done(function(response) {
			
			var result = $.parseJSON(response);
			
			if (result.status == "OK") {

				var json_obj = $.parseJSON(result.data);
				
				var count = 0
				for (var i in json_obj) {
					count++;
					console.log(json_obj[i].url);
					var link = insert_thumbnail(json_obj[i].screenshot_path, json_obj[i].thumbnail_path, 
						json_obj[i].url, '#more-screenshots');

					link.show();
				}
				if (count > 0) 
					$('#more').slideDown("slow");
			}
		})
	};

	function get_screenshot(url) {

		stop_ajax = false;

		$('#form button#go').prop("disabled", true);
		$('#form input#url').prop("disabled", true);
		create_thumb_fake(TIMER_VALUE);
		timer = setInterval(function(){dectimer()}, 1000);

		var selected_width = $.trim($('#customWidth').val());
		if (!selected_width) { 
			selected_width = $('#width').val();
		}

		var opt_cached = 0;
		if ($("#cached input").is(":checked"))
			var opt_cached = 1;

		$.ajax({
			type: 		"POST",
			url: 		'ajax/get-screenshot-id/',
			data: 		{url:url, width:selected_width, cached:opt_cached},
			timeout: 	5000
		})
		.done(function(response) {
			
			var result = $.parseJSON(response);
			
			if (result.status == "OK") {
				get_screenshot_data(result.data.task_id, 1, MAX_CALLS)
			}
			else
				ajax_error(result.data);
		})
		.fail(function(jqXHR, textStatus) {
			ajax_error(textStatus);
		})
	};

	function get_screenshot_data(screenshot_id, number_call, max_calls) {

		if (stop_ajax)
			return;

		$.ajax({
			type: 		"POST",
			url: 		  'ajax/get-screenshot-data/',
			data: 		{id: screenshot_id},
			timeout: 	10000
		})
		.done(function(response) {
			var result = $.parseJSON(response);

			if (result.status == "Fail") {
				//$('#count').text(max_calls - number_call);
				if (number_call < max_calls) {
					number_call++;
					setTimeout(function(){
						get_screenshot_data(screenshot_id, number_call, max_calls)
					}, AJAX_INTERVAL);
				}
				else 
					ajax_error(result.data);
			}
			else if (result.status == "OK") {

				$('.thumb-fake').addClass('text-success');
				
				var link = insert_thumbnail(result.data.screenshot_path, result.data.thumbnail_path,
					 result.data.url, "#result");

				$('.thumb-fake').fadeOut("slow", function() {
    				link.fadeIn("slow");
    				stoptimer();
    				$('.thumb-fake').remove()
  				});
				
			 
				$('#form button#go').prop("disabled", false);
				$('#form input#url').prop("disabled", false);
			}
		})
		.fail(function(jqXHR, textStatus) {
			//$('#count').text(max_calls - number_call);
			if (number_call < max_calls) {
				number_call++;
				setTimeout(function(){
					get_screenshot_data(screenshot_id, number_call, max_calls)
				}, AJAX_INTERVAL);
			}
			else 
				ajax_error(textStatus);
		})
	};

	function ajax_error(error_msg) {
		stoptimer();
		$('.thumb-fake').remove();
		msg_alert("ERROR " + error_msg);
		$('#form button#go').prop("disabled", false);
		$('#form input#url').prop("disabled", false);
	}

	function msg_alert(text) {

		$('<p>').addClass('msg-alert').addClass('bg-danger').text(text).prependTo($('#msg-container'));
	};

	function create_thumb_fake(count) {
		var thumb_fake = $('<span>').addClass('thumb-fake');
		$('<img>').addClass('img-thumbnail').attr('src', 'static/img/thumb_fake.png').appendTo(thumb_fake);
		$('<span>').attr('id', 'count').text(count).appendTo(thumb_fake);

		thumb_fake.hide().prependTo($('#result')).fadeIn("slow");
	};

	function insert_thumbnail(image_path, thumb_path, alt, container_selector) {

		var link = $('<a>').attr('href', image_path).attr('target', '_blank').attr('rel', 'nofollow')
			.attr('alt', alt).attr('title', alt).hide().prependTo(container_selector)
						
		$('<img/>').attr('src', thumb_path).prependTo(link).addClass("img-thumbnail");

		return link;
	}

	function dectimer() {
		var count = $('#count').text();
		if (count > 0) {
			count--;
			$('#count').text(count);			
		}
		if (count <= 0) {
			setTimeout(function(){ajax_error("")}, 2000);			
		}
	}

	function stoptimer() {
		clearInterval(timer);
		stop_ajax = true;
	}

	$("#form").submit(function(event) {
		
		event.preventDefault();

		$('.msg-alert').remove();
				
		url = $.trim($('#url').val());
		if (url) {
			get_screenshot(url);
		}
	});

	$('#more').hide();
	$('#options-link').click(function(event){
		event.preventDefault();

		var options_container = $('#options-container');
		if (options_container.is(":hidden")) {
			options_container.slideDown();
		} else {
			options_container.slideUp();
		}
	});

	get_other_screenshots(10);

});