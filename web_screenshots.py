import hashlib
import datetime
import logging
import time
import json
import re
from multiprocessing import managers, log_to_stderr
from flask import Flask, render_template, request, url_for, make_response

#from flask_debugtoolbar import DebugToolbarExtension

app = Flask(__name__, static_path='/static')
import sys
sys.stdout = sys.stderr

@app.route("/")
def home():
    return render_template('home.html')

@app.route("/screenshots/<path:file_path>")
def return_screenshot(file_path):

    response = make_response()
    response.headers['Cache-Control'] = 'no-cache'
    response.headers['Content-Type'] = 'image/png'
    response.headers['X-Accel-Redirect'] = '/files/' + file_path

    return response

@app.route("/ajax/get-screenshot-id/",  methods=['POST'])
def get_screenshot_id():

    url = request.form['url']
    width = request.form['width'] if request.form['width'] else '800px'
    cached = request.form['cached']

    exp = re.compile('^[0-9]{3,4}px$')
    if exp.match(width):
        width = width[:-2]
        width = int(width)
        if width < 200:
            width = 200
        elif width > 2800:
            width = 2800
    else:
        result = {
                'status':       'Fail',
                'data':         'Bad width format. Must be 800px (ensure it ends with px)'
            }

        return json.dumps(result)

    print url
    print width
    print cached

    manager = managers.BaseManager(address=('localhost', 50000), authkey='lol')
    manager.register('t_add_task')

    task = {
        'name':         'get_screenshot',
        'args': {
            'url':      url,
            'width':    width,
            'cached':   cached,
        }
    }

    manager.connect()
        
    json_result = manager.t_add_task(task)._getvalue()

    print json_result

    return json_result

@app.route("/ajax/get-screenshot-data/",  methods=['POST'])
def get_screenshot_data():

    manager = managers.BaseManager(address=('localhost', 50000), authkey='lol')
    manager.register('t_get_result_task')

    task = {
        'name':         't_get_result_task',
        'args': {
            'id':       request.form['id']
        }
    }

    manager.connect()

    json_result = manager.t_get_result_task(task)._getvalue()

    print json_result

    return json_result

@app.route("/ajax/get-more-screenshots/",  methods=['POST'])
def get_more_screenshots():

    number = request.form['number']

    manager = managers.BaseManager(address=('localhost', 50000), authkey='lol')
    manager.register('t_get_screenshot_list')

    task = {
        'name':         't_get_screenshot_list',
        'args': {
            'number':   number,
        }
    }

    manager.connect()
        
    json_result = manager.t_get_screenshot_list(task)._getvalue()

    # print json_result

    return json_result


if __name__ == "__main__":

    app.debug = True
    app.config['SECRET_KEY'] = 'La sopa sabe sosa'
    #toolbar = DebugToolbarExtension(app)

    app.run()


