import logging
import colorlog
import time
import multiprocessing
import hashlib
import datetime
import json
import random
import os.path
from multiprocessing import managers, Queue, queues, Value
from selenium import webdriver
from PIL import Image
from pyvirtualdisplay import Display
from apscheduler.schedulers.blocking import BlockingScheduler
#from apscheduler.schedulers.background import BackgroundScheduler

RESULTS_FILE                = 'results.db.txt'
LOGGING_LEVEL               = logging.INFO
TIMEOUT_BROWSER             = 60*60              # 60 min
CLEAN_JOBS_INTERVAL         = 60
CLEAN_RESULTS_INTERVAL      = 60*60
CHECK_OVERBOOKING_INTERVAL  = 1
DELAY                       = 5

class Cleaner(multiprocessing.Process):

    def __init__(self, results_lock):

        super(Cleaner, self).__init__()

        self.log            = multiprocessing.get_logger()
        self.l('Inicializando Cleaner...')

        self.results_lock   = results_lock
        self.scheduler      = BlockingScheduler(logger=self.log)
        
        self.scheduler.add_job(self.clean_results, 'interval', seconds=CLEAN_RESULTS_INTERVAL)

    def clean_results(self):

        self.l('Considerando limpieza...')
        to_clean = []
        cleaned = 0
        now = datetime.datetime.now()
        with self.results_lock:
            with open(RESULTS_FILE, 'r') as f:
                lines = f.readlines()
        
                for i, line in enumerate(lines):
                    result = json.loads(line)
                    date_creation = datetime.datetime.strptime(result['date'], '%d-%m-%Y %H:%M:%S')
                    secs = 60*60*24*30  # 1 month
                    if (now - date_creation).total_seconds() > secs:
                        del lines[i]
                        cleaned += 1
        
            with open(RESULTS_FILE, 'w') as f:
                f.writelines(lines)

        if cleaned:
            self.l('Se han eliminado {} resultados caducados'.format(cleaned))

    def run(self):

        self.clean_results()
        self.scheduler.start()

    def l(self, msg):

        now = datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S')

        self.log.info('({}):{} {}'.format(self.pid, now, msg))


class ProcessLauncher(multiprocessing.Process):

    def __init__(self, tasks_queue, min_jobs, max_jobs, num_jobs_sinc, results_lock):

        super(ProcessLauncher, self).__init__()

        self.log = multiprocessing.get_logger()
        self.l('Inicializando...')
        
        self.name           = 'ProcessLauncher'
        self.tasks_queue    = tasks_queue
        self.min_jobs       = min_jobs
        self.max_jobs       = max_jobs
        self.num_jobs_sinc  = num_jobs_sinc
        self.jobs           = []
        self.results_lock   = results_lock
        self.scheduler      = BlockingScheduler(logger=self.log)

        self.scheduler.add_job(self.check_overbooking, 'interval', seconds=CHECK_OVERBOOKING_INTERVAL)
        self.scheduler.add_job(self.clean_old_jobs, 'interval', seconds=CLEAN_JOBS_INTERVAL)
        
    def run(self):

        self.create_firefox_process()
        self.scheduler.start()

    def check_overbooking(self):

        if self.num_jobs_sinc.value < self.min_jobs:

            self.l('Checking overbooking: JOBS {}, TASKS {}, MIN_JOBS {}'.format(
                self.num_jobs_sinc.value, self.tasks_queue.qsize(), self.min_jobs))
            self.create_firefox_process()

        if self.tasks_queue.qsize() > self.num_jobs_sinc.value and self.num_jobs_sinc.value < self.max_jobs:

            self.l('Checking overbooking: JOBS {}, TASKS {}, MIN_JOBS {}'.format(
                self.num_jobs_sinc.value, self.tasks_queue.qsize(), self.min_jobs))
            self.create_firefox_process()

    def create_firefox_process(self):

        firefox = FirefoxWrapper(self.tasks_queue, self.min_jobs, self.num_jobs_sinc,
            self.results_lock, DELAY)
        #firefox.daemon = True
        firefox.start()
        
        with self.num_jobs_sinc.get_lock():
            self.num_jobs_sinc.value += 1

        self.jobs.append(firefox)

    def clean_old_jobs(self):

        num = 0
        for job in self.jobs:
            if not job.is_alive():
               job.terminate()
               self.jobs.remove(job)
               num += 1

        if num:
            self.l('Limpiando viejos procesos: {} eliminados'.format(num))

        # Se cierra para eliminar zombis
        if not self.jobs:
            self.scheduler.shutdown()

    def l(self, msg):

        now = datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S')

        self.log.info('({}):{} {}'.format(self.pid, now, msg))


class FirefoxWrapper(multiprocessing.Process):

    def __init__(self, tasks_queue, min_jobs, num_jobs_sinc, results_lock, delay):

        super(FirefoxWrapper, self).__init__()
        
        self.log            = multiprocessing.get_logger()
        self.l('Inicializando...')

        self.name           = 'FirefoxWrapper'
        self.tasks_queue    = tasks_queue
        self.min_jobs       = min_jobs
        self.num_jobs_sinc  = num_jobs_sinc
        self.browser        = webdriver.Firefox()
        #self.browser        = webdriver.PhantomJS()
        self.jobs           = []
        self.results_lock   = results_lock 
        self.delay          = delay

        self.interface = {
            'get_screenshot'        : self.get_screenshot,
        }

    def run(self):

        while True:

            task = None
            try:
                task = self.tasks_queue.get(timeout=TIMEOUT_BROWSER)
            
            except queues.Empty:
                with self.num_jobs_sinc.get_lock():
                    if self.num_jobs_sinc.value > self.min_jobs:
                        self.l('Lista vacia. Terminando...')
                        self.browser.quit()
                        self.num_jobs_sinc.value -= 1
                    else:
                        continue 
                
                # time.sleep(10)
                return 
            
            self.l('TASKS: ' + str(self.tasks_queue.qsize()))
            
            if task['name'] in self.interface:
                time.sleep(self.delay)
                self.l('Ejecutando la tarea' + str(task))
                self.interface[task['name']](task)

    def get_screenshot(self, task):

        screenshot = dict()
        screenshot['width']  = task['args']['width']
        # screenshot['name']   = hashlib.md5(str(datetime.datetime.now())).hexdigest() + '.png'
        screenshot['name']   = hashlib.md5(task['args']['url'] + '_' + str(task['args']['width'])).hexdigest() + '.png'
        screenshot['path']   = 'screenshots/' + screenshot['name']

        thumb = dict()
        thumb['name']   = hashlib.md5(task['args']['url'] + '_' + str(task['args']['width']) + '_thumb').hexdigest() + '.png'
        thumb['path']   = 'screenshots/thumbnails/' + thumb['name']

        if task['args']['cached'] == '1' and os.path.isfile(screenshot['path']) and os.path.isfile(thumb['path']):
            print 'CACHEADA'
            self.l('CACHED screenshot de {} en {}'.format(task['args']['url'], screenshot['path']))
        else:
            print 'NO CACHEADA'
            self.l('Getting screenshot de {} en {}'.format(task['args']['url'], screenshot['path']))

            self.browser.set_window_size(screenshot['width'] + 16, 600)
            self.browser.get(task['args']['url'])

            # Enable for PhantomJS
            # body = self.browser.find_element_by_tag_name('body')
            # if body.value_of_css_property('background-color') == 'rgba(0, 0, 0, 0)':
            #     print 'CAMBIO'
            #     self.browser.execute_script("document.body.style.background = 'white'")
        
            # Create thumbnail
            if self.browser.save_screenshot(screenshot['path']):

                img = Image.open(screenshot['path'])
                #img = img.convert("RGB")
                w, h = img.size
                if w < h:
                    img = img.crop((0, 0, w, w))
                    img = img.resize((250, 250), Image.ANTIALIAS)
                else:
                    img = img.crop((0, 0, w, h))
                    img = img.resize((250, h*250/w), Image.ANTIALIAS)

                img.save(thumb['path'])

        # Storing the result
        task['result'] = {
            'screenshot_path'   : screenshot['path'],
            'thumbnail_path'    : thumb['path'],
            #'url'               : task['args']['url']
        }

        self.l('Storing the result for the task {}'.format(task['id']))
        with self.results_lock:
            with open(RESULTS_FILE, 'a') as f:
                f.write(json.dumps(task) + '\n')

    def l(self, msg):

        now = datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S')

        self.log.info('({}):{} {}'.format(self.pid, now, msg))


class FirefoxServer:

    def __init__(self, port = 50000, max_jobs = None):    

        self.log            = self.setup_logger()
        self.l('Inicializando servidor...')

        self.port           = port
        self.min_jobs       = 1     # Necesario para reset y eliminar zombis
        self.num_jobs_sinc  = Value('i', 0)
        self.tasks_queue    = multiprocessing.Queue()
        self.results_lock   = multiprocessing.Lock()    # Lock for RESULTS_FILE
        self.jobs           = []

        if max_jobs:
            self.max_jobs = max_jobs
        else:
            self.max_jobs = multiprocessing.cpu_count() * 2

        self.create_process_launcher()

        self.cleaner = Cleaner(self.results_lock)
        self.cleaner.start()

        self.init_server()

    def create_process_launcher(self):

        self.process_launcher = ProcessLauncher(self.tasks_queue, self.min_jobs, 
            self.max_jobs, self.num_jobs_sinc, self.results_lock)
        self.process_launcher.start()

    def l(self, msg):

        now = datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S')

        self.log.info('{} {}'.format(now, msg))
    
    def init_server(self):

        self.manager = managers.BaseManager(address=('localhost', self.port), authkey='lol')
        
        self.interface = {
            't_add_task'            : self.t_add_task,
            't_get_result_task'     : self.t_get_result_task,
            't_get_screenshot_list' : self.t_get_screenshot_list,
        }

        for f_name, f_callable in self.interface.iteritems():
            self.manager.register(f_name, callable=f_callable)
            self.l('Funcion {} registrada'.format(f_name))

        self.l('Escuchando en localhost:{}...'.format(self.port))
        self.manager.get_server().serve_forever()

    def setup_logger(self):

        formatter = colorlog.ColoredFormatter(
            "%(log_color)s[%(levelname)s/%(processName)s] %(message)s",
            datefmt = None,
            reset   = True,
            log_colors = {
                'DEBUG':    'cyan',
                'INFO':     'green',
                'WARNING':  'yellow',
                'ERROR':    'red',
                'CRITICAL': 'red',
            }
        )
        
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)

        log = multiprocessing.get_logger()
        log.addHandler(handler)

        log.setLevel(LOGGING_LEVEL)
        
        return log

    # TASKS
    def t_add_task(self, task):
        ''' task dict format: {'name':'task_name','args':{'arg1':'value1','arg2':'value2'...} '''
        
        # Necesario para eliminar procesos zombis
        if self.process_launcher.is_alive() == False:
            self.process_launcher.terminate()
            self.create_process_launcher()

        task['date'] = datetime.datetime.now()
        task['id']   = hashlib.md5(str(task['date'])).hexdigest()
        task['date'] = task['date'].strftime('%d-%m-%Y %H:%M:%S')
        try:
            self.tasks_queue.put(task)
        except:
            self.log.warn('ERROR Encolando tarea {name} con argumentos {args!s}'.format(**task))
            result = {
                'status':       'Fail',
                'data': {
                    'task_id':  'Task could not be enqueued'
                }
            }

            print json.dumps(result)

            return json.dumps(result)

        self.l('Encolando tarea {name} con argumentos {args!s}'.format(**task))
        
        result = {
            'status':       'OK',
            'data': {
                'task_id':  task['id']
            }
        }

        print json.dumps(result)
        return json.dumps(result)

    def t_get_result_task(self, task):

        with self.results_lock:
            with open(RESULTS_FILE, 'r') as f:
                lines = f.readlines()
                
        for line in lines:
            json_line = json.loads(line)
            if json_line['id'] == task['args']['id'] and json_line['name'] == 'get_screenshot':
                result = {
                    'status':               'OK',
                    'data': {
                        'id':               json_line['id'],
                        'screenshot_path':  json_line['result']['screenshot_path'],
                        'thumbnail_path':   json_line['result']['thumbnail_path'],
                        'url':              json_line['args']['url']               
                    }
                }

                self.l('Resultado a {}: {}'.format(task['args']['id'], json.dumps(result)))
                
                print json.dumps(result)
                return json.dumps(result)

        result = {
            'status':       'Fail',
            'data': {
                'task_id':  'Task result does not exist yet'
            }
        }
        
        print json.dumps(result)
        return json.dumps(result)

    def t_get_screenshot_list(self, task):

        with self.results_lock:
            with open(RESULTS_FILE, 'r') as f:
                screenshots = f.readlines()

        number = int(task['args']['number'])
        if len(screenshots) <= number:
            selected = screenshots
        else:
            selected = random.sample(screenshots, number)

        to_send = []
        for img in selected:
            img = json.loads(img)
            d = {
                'url':              img['args']['url'],
                'thumbnail_path':   img['result']['thumbnail_path'],
                'screenshot_path':  img['result']['screenshot_path']    
            }
            to_send.append(d)


        result = {
            'status':       'OK',
            'data':         json.dumps(to_send)
        }

        #self.l('Devolviendo screenshots de ejemplo {}'.format(to_send))

        return json.dumps(result)

        

if __name__ == '__main__':
    display = Display(visible=0, size=(1024, 768))
    display.start()
    FirefoxServer()
    display.stop()
